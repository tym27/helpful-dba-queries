select pid
     , leader_pid
     , age(now(), xact_start) as xact_age
     , age(now(), query_start) as query_age
     , datname
     , usename
     , application_name
     , left(query, 25) || '..' || right(query, 25) as query_snippet
     , wait_event_type
     , wait_event
     , state
  from pg_stat_activity
-- exclude this query from result
 where pid != pg_backend_pid()
 order by xact_start nulls last
